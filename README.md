# Drishti: Guiding End-Users in the I/O Optimization Journey

Jean Luca Bez, Hammad Ather, Suren Byna
Lawrence Berkeley National Laboratory

This repository contains the logs and analysis of our PDSW 2022 paper:

https://gitlab.com/jeanbez/pdsw-2022

## Drishti

Drishti is a command-line tool to guide end-users in optimizing I/O in their applications by detecting typical I/O performance pitfalls and providing a set of recommendations. You can check it out at:

https://github.com/hpc-io/drishti

```
git clone https://github.com/hpc-io/drishti
```

You need to have Darshan (and pyDarshan), Python >= 3.6, and install some required libraries:

```
pip install -r requirements.txt
```

To easily install Drishti:

```
pip install .
```

You will then be able to run Drishti by providing any ``.darshan`` file as input:

```
drishti my-application-profile.darshan
```

## Contents

In this paper, we conducted two sets of experiments: 

- We have focused on two use-cases (OpenPMD and E2E kernel), considering profiling logs before and after applying I/O optimization techniques
- We have summarized the triggered insights in over 100,000 .darshan logs from Cori

We provide the raw Darshan files for our use-cases in ``examples`` directory. Due to privacy concerns, we can only share a .csv file containing one job step per line and whether or not a given insight was triggered. Each column represents each one of the insights Drishti detected.
